{% set settings = salt['pillar.get']('apache') %} # as defined in pillars/apache/
include:
  - apache.default

{% for website in settings.websites %}

{% set base = settings.default_virtual_base + '/' + website.name %}
{% set log = base + '/' + settings.default_virtual_log %}
{% set root = base + '/' + settings.default_virtual_root %}
{% set tmp = base + '/' + settings.default_virtual_tmp %}

{{ base }}:
  file.directory:
    - name: '{{ base }}'
    - user: root
    - group: root
    - mode: '0755'
    - makedirs: True # Like running mkdir -p

{{ root }}:
  file.directory:
    - name: '{{ root }}'
    - user: www-data # you could have here a custom username or uid, stored under website.uid for example
    - group: www-data
    - dir_mode: '0770'
    - require:
      - file: {{ base }}
    - recurse:
      - user
      - group

{{ tmp }}:
  file.directory:
    - name: '{{ tmp }}'
    - user: www-data
    - group: www-data
    - dir_mode: '0770'
    - require:
      - file: {{ root }}
    - recurse:
      - group

{{ log }}:
  file.directory:
    - name: '{{ log }}'
    - user: www-data # has to have with the same username as the web server
    - group: adm
    - dir_mode: '0770'
    - require:
      - file: {{ root }}

/etc/apache2/sites-available/{{ website.name }}.conf:
  file.managed:
    - source: salt://filebase/apache/site_available_template.conf # will also be used in salt/apache/websites.sls
    - mode: '0644'
    - user: root
    - group: root
    - template: jinja
    - watch_in:
      - service: apache2
    - require:
      - pkg: apache2
      - file: {{ log }}
      - file: {{ tmp }}
      - file: {{ root }}
    - defaults:
      fqdn: {{ website.name }}
      settings: {{ settings }}
      website: {{ website }}

/etc/apache2/sites-enabled/{{ website.name }}.conf:
  file.symlink:
    - name: /etc/apache2/sites-enabled/{{ website.name }}.conf
    - target: /etc/apache2/sites-available/{{ website.name }}.conf
    - force: True
    - require:
      - file: /etc/apache2/sites-available/{{ website.name }}.conf
    - watch_in:
      - service: apache2

{% if 'ssl' in website and website.ssl %}
/etc/ssl/salt-managed/certs/{{ website.name }}.crt:
  file.managed:
    - source: salt://filebase/apache/ssl/certs/{{ website.name }}.crt
    - user: root
    - group: root
    - mode: 0644
    - require:
      - pkg: apache2
      - file: /etc/ssl/salt-managed/certs
    - watch_in:
      - service: apache2

/etc/ssl/salt-managed/keys/{{ website.name }}.key:
  file.managed:
    - source: salt://filebase/apache/ssl/keys/{{ website.name }}.key
    - user: root
    - group: root
    - mode: 0400
    - watch_in:
      - service: apache2
    - require:
      - pkg: apache2
      - file: /etc/ssl/salt-managed/keys
{% endif %}

{% endfor %}

