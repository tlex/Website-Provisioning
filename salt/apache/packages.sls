apache2:
  pkg.installed:
    - pkgs:
      - apache2
      - apache2-utils
  service:
    - running
    - watch.pkg:
      - pkgs:
        - apache2
    - require:
      - pkg: apache2
 
mod_rewrite:
  apache_module.enabled:
    - name: rewrite
    - require:
      - pkg: apache2
    - watch_in:
      - service: apache2
 
mpm_event:
  apache_module.disabled:
    - name: mpm_event
    - require:
      - pkg: apache2
    - watch_in:
      - service: apache2
 
mpm_worker:
  apache_module.disabled:
    - name: mpm_worker
    - require:
      - pkg: apache2
    - watch_in:
      - service: apache2
 
mpm_prefork:
  apache_module.enabled:
    - name: mpm_prefork
    - require:
      - pkg: apache2
      - apache_module: mpm_event
      - apache_module: mpm_worker
    - watch_in:
      - service: apache2
