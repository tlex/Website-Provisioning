{% set localhost = salt['grains.get']('host') %}
 
include:
  - apache.{{ localhost }}

apache:
  default_host_root: /var/www
  default_host_log: /var/log/apache2
  default_host_enable_ssl: True
  default_virtual_base: /www # the website name will be appended to this
  default_virtual_log: logs # the folder will be in form /www/f.q.d.n/logs
  default_virtual_root: htdocs # the folder will be in form /www/f.q.d.n/logs
  default_virtual_tmp: tmp # the folder will be in form /www/f.q.d.n/tmp
  document_root_options: '-Indexes +FollowSymLinks -MultiViews'
  server_admin: root@localdomain
  server_signature: 'off'
  server_tokens: 'Prod'
  ssl_enable_bundle: True # If True it will manage the /etc/ssl/bundle.crt file
  hsts_max_age: 31536000 # about a year
  access_log_format_ssl: '"%t %h %{SSL_PROTOCOL}x %{SSL_CIPHER}x \"%r\" %b"'
  access_log_format: combined
  access_log_merge_ssl: True
