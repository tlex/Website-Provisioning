apache:
  default_virtual_base: '/sites'
  websites:
    - name: bitleader.one
      aliases:
        - www.bitleader.one
