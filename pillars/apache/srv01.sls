apache:
  hsts_max_age: 15768000 # about half a year
  websites:
    - name: e-tel.eu
      aliases:
        - www.e-tel.eu
      ssl: True
      force_ssl: True
      hsts: True
    - name: www.bitleader.com
      aliases:
        - bitleader.com
      ssl: True
